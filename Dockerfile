FROM openjdk:8-jre-alpine
COPY target/Api-Investimentos-*.jar appinvestimento.jar
CMD ["java", "-jar", "appinvestimento.jar"]