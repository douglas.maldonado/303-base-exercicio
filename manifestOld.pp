$svc_conf_template = @(END)
[Unit]
Description=Api de Investimento

[Service]
User=ubuntu

#path to executable.
#executable is a bash script which calls jar file
ExecStart=/usr/bin/java -javaagent:/home/ubuntu/appagent/javaagent.jar -jar /home/ubuntu/appinvestimento/appinvestimento.jar
SuccessExitStatus=143
TimeoutStopSec=10
Restart=on-failure
RestartSec=5
[Install]
WantedBy=multi-user.target
END

$svc_authorized = @(END)
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC2ya3ziLkqnaO/r3TpNyJ9U1zBEBcxhbAcwVaGHPjZ5GENea78Qm6iZJdlsPzumcGRnf/yGuix++SPHFIadW5YcYLJN1eKdVOKhHwhezLtu21TxVZ3Bpi2DRE5I3jbqB1ZM5aDTHxykLJfy1jZrbAVUVKiEt44+OBtDNqwFc+7WM0CiLBKe7aZ8iH1bql8rynEDm9ZuO3FYwuEsycBDX3EbuH3tUjeBIS/yI+gUcEau+5hqt5TVHCHCB0lf7H4NK+PYRbPk62IViP4yXn+MEz/HkyrANyS/ROS08JLXpz+1s/gTuyO1P9XsmfFPtr8YphlTxkc5p7hk7YSAq3rUH6f douglas-maldonado-keypair

ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDH5mJ8GIQ3eOjal1tw4mviXXoBETtAaHIuXNrvCpD3qHLSSMlYjeDOG1xepa11CHTNJBcNlxeRGqT3to4EGKCqes3Y9IlaizZLr16hXHf+sH4zlGxz8DStJYk4cUSXE5ynj1wM11xGv+0bDtkkR1R9WoQTzf7Jt9qf4blWs0Y9Hem3Q4XEU6KJ7XPJwh/1FTtDy0gm0qMF2Nfcxl0P/WhufBlr10nKcQb88nM7RZSDT+/iUFrgJOiaLkqKBYcLSZRM1j7FtKGHAGz/DCwo6kEbJu0xAJJb1Sg80Zn+yKcFeWj+sHJFMsnn7fyO06YAHMxfV8WW5uCmiG0CdllhhjO3oLP2elVNh28JAbkuxNUQWjG02eeX3BkHw4Wxpq8H2wKyLQI15ADu7+A49Yx54eLHAm0oBkSyDCUrY0pU4K9tF40zjMkExRx0OkQAYH7v9/sfrt1VefvzL1yZx++mtx+Ta9NOxfTHN3u3fEm1TxJdDmYcarq74cwOOMHmUcveUc0= a2@localhost.localdomain

ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDvNfe5q8SfXeGyl6cB2vR4mo7pHq30sjPgMrhP05UF2gH3bgz7dGREpb6caCGJo8gpQXofeEI0wdqLw0KbmIuqSMpT4yQQHWr6VqWbDaK2XQutHJkElGQMTCBADwT4bGt8b2eNV8s581g/RS01q4L7F9iSkMzSms3BM63FkZHZAOlOTHmRnN3Ju39MolmjGPwb6SpUFYpMIZLic1vKTe4p1L3PQL4czIJxpssE283iOeRiAceD/Xaly4mgVqdxmMEm3gZo5PzcPMu5Y08M6Ur0vJR5i4IVSSheBhzcKLshuvTBt5bobYtG1BWatQIpbKoCO6Uqnf/7U7FvPHJTYsJb jenkins@banana
END

class { 'java':
package => 'openjdk-8-jdk',
}
file { '/etc/systemd/system/appinvestimento.service':
ensure => file,
content => inline_epp($svc_conf_template),
}
file { '/home/ubuntu/.ssh/authorized_keys':
ensure => file,
content => inline_epp($svc_authorized),
}
service { 'appinvestimento':
ensure => running,
}
class{"nginx": }
nginx::resource::server { 'appinvestimento':
server_name => ['_'],
listen_port => 80,
proxy => 'http://localhost:8080',
}
file { "/etc/nginx/conf.d/default.conf":
ensure => absent,
notify => Service['nginx'],
}